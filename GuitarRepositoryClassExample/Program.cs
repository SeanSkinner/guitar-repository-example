﻿// See https://aka.ms/new-console-template for more information

using GuitarRepositoryClassExample.Models;
using GuitarRepositoryClassExample.Repositories;
using Microsoft.Data.SqlClient;

var guitarRepository = new GuitarRepository { ConnectionString = GetConnectionString() };

guitarRepository.Update(new Guitar(6, "Jazzmaster Deluxe", 1));

var guitar = guitarRepository.GetById(6);

Console.WriteLine(guitar.Description);


//var allGuitars = guitarRepository.GetAll();

//foreach (var guitar in allGuitars)
//{
//    Console.WriteLine(guitar.Description);
//}

//var brand = guitarRepository.GetBrandNameByGuitarId(4);

//Console.WriteLine(brand.GuitarBrandName);



static string GetConnectionString()
{
    var builder = new SqlConnectionStringBuilder
    {
        DataSource = "localhost\\SQLEXPRESS",
        InitialCatalog = "Guitars",
        IntegratedSecurity = true,
        TrustServerCertificate = true
    };

    return builder.ConnectionString;
}