﻿using GuitarRepositoryClassExample.Models;

namespace GuitarRepositoryClassExample.Repositories
{
    public interface IGuitarRepository: ICrudRepository<Guitar, int>
    {
        GuitarBrand GetBrandNameByGuitarId(int guitarId);
    }
}
