﻿using GuitarRepositoryClassExample.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuitarRepositoryClassExample.Repositories
{
    public class GuitarRepository : IGuitarRepository
    {
        public string ConnectionString { get; set; } = string.Empty;
        public void Add(Guitar entity)
        {
            using var connection = new SqlConnection(ConnectionString);
            connection.Open();
            var sql = "INSERT INTO Guitar (GuitarDescription, BrandId) VALUES (@GuitarDescription, @BrandId)";
            using var command = new SqlCommand(sql, connection);
            command.Parameters.AddWithValue("@GuitarDescription", entity.Description);
            command.Parameters.AddWithValue("@BrandId", entity.BrandId);
            command.ExecuteNonQuery();
        }

        public void Delete(int id)
        {
            using var connection = new SqlConnection(ConnectionString);
            connection.Open();
            var sql = "DELETE FROM Guitar WHERE GuitarId = @GuitarId";
            using var command = new SqlCommand(sql, connection);
            command.Parameters.AddWithValue("@GuitarId", id);
            command.ExecuteNonQuery();
        }

        public IEnumerable<Guitar> GetAll()
        {
            using var connection = new SqlConnection(ConnectionString);
            connection.Open();
            var sql = "SELECT GuitarId, GuitarDescription, BrandId FROM Guitar";
            using var command = new SqlCommand(sql, connection);
            using SqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                yield return new Guitar(
                    reader.GetInt32(0),
                    reader.GetString(1),
                    reader.GetInt32(2)
                    );
            }
        }

        public GuitarBrand GetBrandNameByGuitarId(int guitarId)
        {
            using var connection = new SqlConnection(ConnectionString);
            connection.Open();
            var sql = "SELECT b.BrandName " +
                "FROM Brand AS b " +
                "INNER JOIN Guitar AS g ON g.BrandId = b.BrandId AND g.GuitarId = @GuitarId";
            using var command = new SqlCommand(sql, connection);
            command.Parameters.AddWithValue("@GuitarId", guitarId);
            using SqlDataReader reader = command.ExecuteReader();

            var result = new GuitarBrand();

            while (reader.Read())
            {
                result = new GuitarBrand(reader.GetString(0));
            }

            return result;
        }

        public Guitar GetById(int id)
        {
            using var connection = new SqlConnection(ConnectionString);
            connection.Open();
            var sql = "SELECT GuitarId, GuitarDescription, BrandId FROM Guitar WHERE GuitarId = @GuitarId";
            using var command = new SqlCommand(sql, connection);
            command.Parameters.AddWithValue("@GuitarId", id);
            using var reader = command.ExecuteReader();

            var result = new Guitar();

            while (reader.Read())
            {
                result = new Guitar(
                    reader.GetInt32(0),
                    reader.GetString(1),
                    reader.GetInt32(2)
                    );
            }

            return result;
        }

        public void Update(Guitar entity)
        {
            using var connection = new SqlConnection(ConnectionString);
            connection.Open();
            var sql = "UPDATE Guitar SET GuitarDescription = @GuitarDescription, BrandId = @BrandId " +
                "WHERE GuitarId = @GuitarId";
            using var command = new SqlCommand(sql, connection);
            command.Parameters.AddWithValue("@GuitarDescription", entity.Description);
            command.Parameters.AddWithValue("@BrandId", entity.BrandId);
            command.Parameters.AddWithValue("@GuitarId", entity.Id);
            command.ExecuteNonQuery();
        }
    }
}
