﻿using GuitarRepositoryClassExample.Models;

namespace GuitarRepositoryClassExample.Repositories
{
    public class BrandRepository : IBrandRepository
    {
        public void Add(Brand entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Brand> GetAll()
        {
            throw new NotImplementedException();
        }

        public Brand GetById(int id)
        {
            throw new NotImplementedException();
        }

        public void Update(Brand entity)
        {
            throw new NotImplementedException();
        }
    }
}
