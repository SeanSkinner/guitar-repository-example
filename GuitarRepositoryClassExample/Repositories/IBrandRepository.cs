﻿using GuitarRepositoryClassExample.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuitarRepositoryClassExample.Repositories
{
    public interface IBrandRepository: ICrudRepository<Brand, int>
    {
    }
}
