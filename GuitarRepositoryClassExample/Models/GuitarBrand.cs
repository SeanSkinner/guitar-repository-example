﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuitarRepositoryClassExample.Models
{
    public readonly record struct GuitarBrand(string GuitarBrandName);
}
